module gitlab.com/srcgo/changelog/v2

go 1.23.2

require github.com/urfave/cli/v2 v2.27.5

require github.com/yuin/goldmark v1.7.8

require (
	github.com/Masterminds/semver/v3 v3.3.1
	github.com/cpuguy83/go-md2man/v2 v2.0.5 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	gitlab.com/srcgo/sc v1.0.1
)
