package actions

import (
	_ "embed"

	"github.com/Masterminds/semver/v3"
	"github.com/urfave/cli/v2"
	"gitlab.com/srcgo/changelog/v2/changelog"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/flags"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/utils"
)

//go:embed update.txt
var update_description string

var Update = struct {
	Description func() string
	Flags       []cli.Flag
	Command     func(*cli.Context) error
}{
	Description: func() string { return update_description },
	Flags: []cli.Flag{
		&cli.GenericFlag{
			Name:    "version",
			Aliases: []string{"v"},
			Usage:   "version to update",
			Value:   &flags.Version{},
		},
		&cli.GenericFlag{
			Name:    "target",
			Aliases: []string{"t"},
			Usage:   "target version",
			Value:   &flags.Version{},
		},
		&cli.GenericFlag{
			Name:    "increment",
			Aliases: []string{"i"},
			Usage:   "target version increment",
			Value:   &flags.Increment{},
		},
		&cli.GenericFlag{
			Name:    "refs",
			Aliases: []string{"r"},
			Usage:   "set reference format (default: md)",
			Value:   &flags.Refs{},
		},
		&cli.BoolFlag{
			Name:    "date",
			Aliases: []string{"d"},
			Usage:   "update date of version",
		},
	},
	Command: func(ctx *cli.Context) error {
		versionFlag := ctx.Generic("version").(*flags.Version)
		targetFlag := ctx.Generic("target").(*flags.Version)
		incrementFlag := ctx.Generic("increment").(*flags.Increment)
		refsFlag := ctx.Generic("refs").(*flags.Refs)
		dateFlag := ctx.Bool("date")
		cl, err := utils.OpenChangelog()

		if err != nil {
			return err
		}

		if !ctx.IsSet("refs") {
			refsFlag = &flags.Refs{Type: changelog.RefMarkdown}
		}

		var vs *changelog.Version

		if !ctx.IsSet("version") {
			for _, ch := range cl.Changes {
				if vs == nil || ch.GetVersion().Semver.GreaterThanEqual(vs.Semver) {
					vs = ch.GetVersion()
				}
			}
		} else {
			for _, ch := range cl.Changes {
				if ch.GetVersion().Semver.Original() == versionFlag.String() {
					vs = ch.GetVersion()
					break
				}
			}
		}

		if vs != nil {
			var err error

			if ctx.IsSet("target") {
				if vs.Semver, err = semver.NewVersion(targetFlag.String()); err != nil {
					return err
				}
			}

			if ctx.IsSet("increment") {
				iv := incrementFlag.Component.Increment(vs.Semver)
				vs.Semver = &iv
			}

			if dateFlag {
				vs.DateModified = utils.Today()
			}
		}

		cl.Sort()
		cl.RefType = refsFlag.Type
		return utils.SaveChangelog(cl)
	},
}
