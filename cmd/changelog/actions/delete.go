package actions

import (
	_ "embed"

	"github.com/urfave/cli/v2"
	"gitlab.com/srcgo/changelog/v2/changelog"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/flags"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/utils"
)

//go:embed delete.txt
var delete_description string

var Delete = struct {
	Description func() string
	Flags       []cli.Flag
	Command     func(*cli.Context) error
}{
	Description: func() string { return delete_description },
	Flags: []cli.Flag{
		&cli.GenericFlag{
			Name:    "version",
			Aliases: []string{"v"},
			Usage:   "version to delete (multiple allowed)",
			Value:   &flags.Versions{},
		},
		&cli.GenericFlag{
			Name:    "entry",
			Aliases: []string{"e"},
			Usage:   "entry to delete (multiple allowed)",
			Value:   &flags.Entries{},
		},
	},
	Command: func(ctx *cli.Context) error {
		versionFlag := ctx.Generic("version").(*flags.Versions)
		entryFlag := ctx.Generic("entry").(*flags.Entries)
		cl, err := utils.OpenChangelog()

		if err != nil {
			return err
		}

		l := cl.Changes.Len()
		removed := map[*changelog.Change]struct{}{}

		for _, e := range entryFlag.Values {
			for i, c := range cl.Changes {
				if e.Id == l-i {
					removed[c] = struct{}{}
					break
				}
			}
		}

		for _, v := range versionFlag.Values {
			for _, c := range cl.Changes {
				if v.Semver.String() == c.GetVersion().Semver.String() {
					removed[c] = struct{}{}
				}
			}
		}

		oldChanges := cl.Changes
		cl.Changes = []*changelog.Change{}

		for _, ch := range oldChanges {
			if _, has := removed[ch]; !has {
				cl.Changes = append(cl.Changes, ch)
			}
		}

		cl.Sort()
		return utils.SaveChangelog(cl)
	},
}
