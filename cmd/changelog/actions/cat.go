package actions

import (
	_ "embed"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/srcgo/changelog/v2/changelog"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/flags"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/utils"
)

//go:embed cat.txt
var cat_description string

var Cat = struct {
	Description func() string
	Flags       []cli.Flag
	Command     func(*cli.Context) error
}{
	Description: func() string { return cat_description },
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:    "start",
			Aliases: []string{"s"},
			Usage:   "start version index (negative to select from end)",
			Value:   0,
		},
		&cli.IntFlag{
			Name:    "end",
			Aliases: []string{"e"},
			Usage:   "end version index (negative to select from end)",
			Value:   -1,
		},
		&cli.GenericFlag{
			Name:    "refs",
			Aliases: []string{"r"},
			Usage:   "set reference format (default: md)",
			Value:   &flags.Refs{},
		},
		&cli.BoolFlag{
			Name:    "date",
			Aliases: []string{"d"},
			Usage:   "update date of latest version",
		},
	},
	Command: func(ctx *cli.Context) error {
		startFlag := ctx.Int("start")
		endFlag := ctx.Int("end")
		refsFlag := ctx.Generic("refs").(*flags.Refs)
		dateFlag := ctx.Bool("date")
		cl, err := utils.OpenChangelog()

		if err != nil {
			return err
		}

		if !ctx.IsSet("refs") {
			refsFlag = &flags.Refs{Type: changelog.RefMarkdown}
		}

		var vs *changelog.Version
		l := 0

		for _, ch := range cl.Changes {
			if ch.GetVersion() != vs {
				vs = ch.GetVersion()
				l++
			}
		}

		if l == 0 {
			return nil
		}

		start := (l + startFlag%l) % l
		end := ((l + endFlag%l) % l) + 1
		section := []*changelog.Change{}

		if start < end {
			cl.Sort()
			vs = nil
			i := 0

			for _, ch := range cl.Changes {
				if ch.GetVersion() != vs {
					vs = ch.GetVersion()
					i++
				}

				if i > end {
					break
				}

				if i > start {
					section = append(section, ch)
				}
			}

			if len(section) > 0 {
				if dateFlag {
					section[0].GetVersion().DateModified = utils.Today()
				}

				cl.RefType = refsFlag.Type
				cl.Changes = section
				_, err := cl.WriteTo(os.Stdout)
				return err
			}
		}

		return nil
	},
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
