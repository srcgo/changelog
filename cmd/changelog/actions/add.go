package actions

import (
	_ "embed"
	"fmt"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/urfave/cli/v2"
	"gitlab.com/srcgo/changelog/v2/changelog"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/flags"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/utils"
)

var changeFlags = []string{
	"added",
	"changed",
	"fixed",
}

//go:embed add.txt
var add_description string

//go:embed add.entry.txt
var add_entry_description string

//go:embed add.note.txt
var add_note_description string

var Add = struct {
	Description func() string
	Entry       struct {
		Description func() string
		Flags       []cli.Flag
		Command     func(*cli.Context) error
	}
	Note struct {
		Description func() string
		Flags       []cli.Flag
		Command     func(*cli.Context) error
	}
}{
	Description: func() string { return add_description },
	Entry: struct {
		Description func() string
		Flags       []cli.Flag
		Command     func(*cli.Context) error
	}{
		Description: func() string { return add_entry_description },
		Flags: []cli.Flag{
			&cli.GenericFlag{
				Name:     "type",
				Aliases:  []string{"t"},
				Usage:    "change-type",
				Required: true,
				Value:    &flags.Type{},
			},
			&cli.StringFlag{
				Name:     "message",
				Aliases:  []string{"m"},
				Usage:    "change-message",
				Required: true,
			},
			&cli.GenericFlag{
				Name:    "version",
				Aliases: []string{"v"},
				Usage:   "target version (defaults to latest)",
				Value:   &flags.Version{},
			},
			&cli.GenericFlag{
				Name:    "increment",
				Aliases: []string{"i"},
				Usage:   "target version increment",
				Value:   &flags.Increment{},
			},
			&cli.GenericFlag{
				Name:    "refs",
				Aliases: []string{"r"},
				Usage:   "set reference format (default: md)",
				Value:   &flags.Refs{},
			},
			&cli.BoolFlag{
				Name:    "date",
				Aliases: []string{"d"},
				Usage:   "update date of version",
			},
		},
		Command: func(ctx *cli.Context) error {
			typeFlag := ctx.Generic("type").(*flags.Type)
			messageFlag := ctx.String("message")
			versionFlag := ctx.Generic("version").(*flags.Version)
			incrementFlag := ctx.Generic("increment").(*flags.Increment)
			refsFlag := ctx.Generic("refs").(*flags.Refs)
			dateFlag := ctx.Bool("date")
			cl, err := utils.OpenChangelog()

			if err != nil {
				return err
			}

			if !ctx.IsSet("refs") {
				refsFlag = &flags.Refs{Type: changelog.RefMarkdown}
			}

			var vs *changelog.Version

			if !ctx.IsSet("version") {
				for _, ch := range cl.Changes {
					if vs == nil || ch.GetVersion().Semver.GreaterThanEqual(vs.Semver) {
						vs = ch.GetVersion()
					}
				}

				if vs == nil {
					vs = &changelog.Version{Semver: semver.New(0, 0, 0, "", "")}
				}

				if ctx.IsSet("increment") {
					iv := incrementFlag.Component.Increment(vs.Semver)
					vs = &changelog.Version{Semver: &iv, DateModified: vs.DateModified}
				}
			} else {
				sv := versionFlag.Semver
				var dm time.Time

				if ctx.IsSet("increment") {
					iv := incrementFlag.Component.Increment(versionFlag.Semver)
					sv = &iv

					for _, ch := range cl.Changes {
						if dm.IsZero() && ch.GetVersion().Semver.Original() == versionFlag.String() {
							dm = ch.GetVersion().DateModified
						}

						if vs == nil && ch.GetVersion().Semver.Original() == iv.String() {
							vs = ch.GetVersion()
						}

						if vs != nil && !dm.IsZero() {
							break
						}
					}
				} else {
					for _, ch := range cl.Changes {
						if ch.GetVersion().Semver.Original() == versionFlag.String() {
							vs = ch.GetVersion()
							break
						}
					}
				}

				if vs == nil {
					if dm.IsZero() {
						dm = utils.Today()
					}

					vs = &changelog.Version{Semver: sv, DateModified: dm}
				}
			}

			if dateFlag {
				vs.DateModified = utils.Today()
			}

			cl.AddChange(&changelog.Change{Version: vs, Type: typeFlag.Type, Message: []byte(messageFlag)})
			cl.Sort()
			cl.RefType = refsFlag.Type
			return utils.SaveChangelog(cl)
		},
	},
	Note: struct {
		Description func() string
		Flags       []cli.Flag
		Command     func(*cli.Context) error
	}{
		Description: func() string { return add_note_description },
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "message",
				Aliases:  []string{"m"},
				Usage:    "note-message",
				Required: true,
			},
			&cli.GenericFlag{
				Name:     "entry",
				Aliases:  []string{"e"},
				Usage:    "entry to add note to",
				Value:    &flags.Entry{},
				Required: true,
			},
			&cli.GenericFlag{
				Name:    "refs",
				Aliases: []string{"s"},
				Usage:   "set reference format (default: md)",
				Value:   &flags.Refs{},
			},
			&cli.BoolFlag{
				Name:    "date",
				Aliases: []string{"d"},
				Usage:   "update date of version",
			},
		},
		Command: func(ctx *cli.Context) error {
			messageFlag := ctx.String("message")
			entryFlag := ctx.Generic("entry").(*flags.Entry)
			refsFlag := ctx.Generic("refs").(*flags.Refs)
			dateFlag := ctx.Bool("date")
			cl, err := utils.OpenChangelog()

			if err != nil {
				return err
			}

			if !ctx.IsSet("refs") {
				refsFlag = &flags.Refs{Type: changelog.RefMarkdown}
			}

			var ch *changelog.Change
			l := cl.Changes.Len()

			for i, c := range cl.Changes {
				if entryFlag.Id == l-i {
					ch = c
					break
				}
			}

			if ch == nil {
				return fmt.Errorf("no such entry '%s'", entryFlag.String())
			}

			ch.AddChange(&changelog.Change{Message: []byte(messageFlag)})

			if dateFlag {
				ch.GetVersion().DateModified = utils.Today()
			}

			cl.Sort()
			cl.RefType = refsFlag.Type
			return utils.SaveChangelog(cl)
		},
	},
}
