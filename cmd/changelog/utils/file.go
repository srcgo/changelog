package utils

import (
	"errors"
	"io/fs"
	"os"

	"gitlab.com/srcgo/changelog/v2/changelog"
)

const ChangelogEnvVar = "CHANGELOG_FILE"
const ChangelogDefaultFilename = "CHANGELOG.md"

func ChangelogFilename() string {
	if fn := os.Getenv(ChangelogEnvVar); len(fn) > 0 {
		return fn
	}

	return ChangelogDefaultFilename
}

func OpenChangelog() (*changelog.Changelog, error) {
	cl := changelog.NewChangeLog(nil, nil)
	f, err := os.Open(ChangelogFilename())

	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			err = nil
		}

		return changelog.NewChangeLog([]byte("Changelog"), nil), err
	}

	defer f.Close()
	_, err = cl.ReadFrom(f)
	return cl, err
}

func SaveChangelog(cl *changelog.Changelog) error {
	if f, err := os.OpenFile(ChangelogFilename(), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666); err != nil {
		return err
	} else {
		defer f.Close()
		_, err = cl.WriteTo(f)
		return err
	}
}
