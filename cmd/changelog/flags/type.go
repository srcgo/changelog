package flags

import (
	"fmt"
	"strings"

	"gitlab.com/srcgo/changelog/v2/changelog"
)

type Type struct {
	Type  changelog.ChangeType
	value string
}

func (t *Type) Set(s string) error {
	c := strings.ToLower(s)

	switch c {
	case "added":
		t.Type = changelog.Added
	case "changed":
		t.Type = changelog.Changed
	case "fixed":
		t.Type = changelog.Fixed
	case "deprecated":
		t.Type = changelog.Deprecated
	case "removed":
		t.Type = changelog.Removed
	case "security":
		t.Type = changelog.Security
	default:
		return fmt.Errorf("invalid change type '%s'", s)
	}

	t.value = c
	return nil
}

func (t *Type) String() string {
	return t.value
}
