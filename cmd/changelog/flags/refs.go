package flags

import (
	"fmt"
	"strings"

	"gitlab.com/srcgo/changelog/v2/changelog"
)

type RefType int

type Refs struct {
	Type  changelog.RefType
	value string
}

func (r *Refs) Set(s string) error {
	c := strings.ToLower(s)

	switch c {
	case "none":
		r.Type = changelog.RefNone
	case "md":
		r.Type = changelog.RefMarkdown
	case "html":
		r.Type = changelog.RefHtml
	default:
		return fmt.Errorf("invalid ref type '%s'", s)
	}

	r.value = c
	return nil
}

func (r *Refs) String() string {
	return r.value
}
