package flags

import (
	"fmt"
	"regexp"
)

var entryExp = regexp.MustCompile(`^#[1-9]\d*$`)

type Entry struct {
	Id    int
	value string
}

type Entries struct {
	Values []*Entry
}

func (e *Entry) Set(s string) error {
	if _, err := fmt.Sscanf(s, "%d", &e.Id); err != nil {
		if !entryExp.MatchString(s) {
			return fmt.Errorf("invalid entry id '%s'", s)
		}

		if _, err := fmt.Sscanf(s[1:], "%d", &e.Id); err != nil {
			return err
		}
	} else if e.Id < 1 {
		return fmt.Errorf("invalid entry id '%s'", s)
	}

	e.value = s
	return nil
}

func (e *Entry) String() string {
	return e.value
}

func (es *Entries) Set(s string) error {
	var e Entry

	if err := e.Set(s); err != nil {
		return err
	} else {
		es.Values = append(es.Values, &e)
	}

	return nil
}

func (es *Entries) String() string {
	return fmt.Sprintf("%v", es.Values)
}
