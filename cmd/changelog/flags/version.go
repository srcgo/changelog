package flags

import (
	"fmt"

	"github.com/Masterminds/semver/v3"
)

type Version struct {
	Semver *semver.Version
}

type Versions struct {
	Values []*Version
}

func (v *Version) Set(s string) error {
	if sv, err := semver.NewVersion(s); err != nil {
		return err
	} else {
		v.Semver = sv
	}

	return nil
}

func (v *Version) String() string {
	if v.Semver == nil {
		return ""
	}

	return v.Semver.String()
}

func (vs *Versions) Set(s string) error {
	var v Version

	if err := v.Set(s); err != nil {
		return err
	} else {
		vs.Values = append(vs.Values, &v)
	}

	return nil
}

func (vs *Versions) String() string {
	return fmt.Sprintf("%v", vs.Values)
}
