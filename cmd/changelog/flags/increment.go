package flags

import (
	"fmt"
	"strings"

	"github.com/Masterminds/semver/v3"
)

type VersionComponent int

const (
	Patch VersionComponent = iota
	Minor
	Major
)

func (vc *VersionComponent) Increment(v *semver.Version) semver.Version {
	switch *vc {
	case Patch:
		return v.IncPatch()
	case Minor:
		return v.IncMinor()
	case Major:
		return v.IncMajor()
	}

	panic("invalid VersionComponent")
}

type Increment struct {
	Component VersionComponent
	value     string
}

func (i *Increment) Set(s string) error {
	c := strings.ToLower(s)

	switch c {
	case "patch":
		i.Component = Patch
	case "minor":
		i.Component = Minor
	case "major":
		i.Component = Major
	default:
		return fmt.Errorf("invalid version component '%s'", s)
	}

	i.value = c
	return nil
}

func (i *Increment) String() string {
	return i.value
}
