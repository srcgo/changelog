package main

import (
	_ "embed"
	"os"
	"path/filepath"

	"github.com/urfave/cli/v2"
	"gitlab.com/srcgo/changelog/v2/cmd/changelog/actions"
	"gitlab.com/srcgo/sc/api/sc"
)

var app *cli.App

//go:embed version.txt
var version string

//go:embed description.txt
var description string

func init() {
	app = &cli.App{
		Usage:                  "manage a changelog file",
		HideHelpCommand:        true,
		UseShortOptionHandling: true,
		Version:                version,
		Description:            description,
		UsageText:              filepath.Base(os.Args[0]) + " [GLOBAL OPTIONS] [COMMAND [COMMAND OPTIONS]]",
		Commands: []*cli.Command{
			{
				Name:                   "add",
				Usage:                  "add an entry or a note",
				Description:            actions.Add.Description(),
				HideHelpCommand:        true,
				UseShortOptionHandling: true,
				UsageText:              filepath.Base(os.Args[0]) + " add [OPTIONS] [COMMAND [COMMAND OPTIONS]]",
				Subcommands: []*cli.Command{
					{
						Name:                   "entry",
						Usage:                  "add a new entry to a version",
						Description:            actions.Add.Entry.Description(),
						HideHelpCommand:        true,
						UseShortOptionHandling: true,
						UsageText:              filepath.Base(os.Args[0]) + " add entry [OPTIONS]",
						Flags:                  actions.Add.Entry.Flags,
						Action:                 actions.Add.Entry.Command,
					},
					{
						Name:                   "note",
						Usage:                  "add a new note to an entry",
						Description:            actions.Add.Note.Description(),
						HideHelpCommand:        true,
						UseShortOptionHandling: true,
						UsageText:              filepath.Base(os.Args[0]) + " add note [OPTIONS]",
						Flags:                  actions.Add.Note.Flags,
						Action:                 actions.Add.Note.Command,
					},
				},
			},
			{
				Name:                   "update",
				Usage:                  "update a version",
				Description:            actions.Update.Description(),
				HideHelpCommand:        true,
				UseShortOptionHandling: true,
				UsageText:              filepath.Base(os.Args[0]) + " update [OPTIONS]",
				Action:                 actions.Update.Command,
				Flags:                  actions.Update.Flags,
			},
			{
				Name:                   "delete",
				Usage:                  "delete versions and/or entries",
				Description:            actions.Delete.Description(),
				HideHelpCommand:        true,
				UseShortOptionHandling: true,
				UsageText:              filepath.Base(os.Args[0]) + " delete [OPTIONS]",
				Action:                 actions.Delete.Command,
				Flags:                  actions.Delete.Flags,
			},
			{
				Name:                   "cat",
				Usage:                  "output the changelog",
				Description:            actions.Cat.Description(),
				HideHelpCommand:        true,
				UseShortOptionHandling: true,
				UsageText:              filepath.Base(os.Args[0]) + " cat [OPTIONS]",
				Action:                 actions.Cat.Command,
				Flags:                  actions.Cat.Flags,
			},
		},
	}
}

func main() {
	sc.W(app.Run(os.Args))
}
