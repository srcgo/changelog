package changelog

import (
	"fmt"
	"strings"

	"github.com/yuin/goldmark/ast"
)

func parseChangelog(cl *Changelog, node ast.Node, source []byte) error {
	cl.Changes = changeCollection{}
	cl.Header = []byte{}
	cl.Description = [][]byte{}
	cl.idMap = map[int]int{}

	if err := parseHeader(cl, node, source); err == nil {
		l := cl.Changes.Len()

		for i, ch := range cl.Changes {
			ch.origId = l - i
		}

		return nil
	} else {
		return err
	}
}

func parseHeader(cl *Changelog, node ast.Node, source []byte) error {
	if h, ok := node.(*ast.Heading); !ok {
		return fmt.Errorf("invalid node kind '%v', expected '%v'", node.Kind(), ast.KindHeading)
	} else {
		if h.Level != 1 {
			return fmt.Errorf("invalid heading level '%v', expected '1'", h.Level)
		}

		cl.Header = h.Lines().Value(source)
		return parseParagraphs(cl, node.NextSibling(), source)
	}
}

func parseParagraphs(cl *Changelog, node ast.Node, source []byte) error {
	if p, ok := node.(*ast.Paragraph); !ok {
		return parseVersion(cl, node, source)
	} else {
		cl.Description = append(cl.Description, p.Lines().Value(source))
		return parseParagraphs(cl, node.NextSibling(), source)
	}
}

func parseVersion(cl *Changelog, node ast.Node, source []byte) error {
	if node == nil {
		return nil // end of ast
	} else if h, ok := node.(*ast.Heading); !ok {
		return fmt.Errorf("invalid node kind '%v', expected '%v'", node.Kind(), ast.KindHeading)
	} else {
		if h.Level != 2 {
			return fmt.Errorf("invalid heading level '%v', expected '2'", h.Level)
		}

		vs := Version{}

		return firstErr(
			func() error { return vs.Parse(h.Lines().Value(source)) },
			func() error { return parseAdded(cl, &vs, node.NextSibling(), source) },
		)
	}
}

func parseAdded(cl *Changelog, vs *Version, node ast.Node, source []byte) error {
	if node == nil {
		return parseVersion(cl, node, source)
	}

	if !pContains(node, "added", source) {
		return parseChanged(cl, vs, node, source)
	}

	if next, err := parseList(cl, vs, Added, node.NextSibling(), source); err != nil {
		return err
	} else {
		return parseChanged(cl, vs, next, source)
	}
}

func parseChanged(cl *Changelog, vs *Version, node ast.Node, source []byte) error {
	if node == nil {
		return parseVersion(cl, node, source)
	}

	if !pContains(node, "changed", source) {
		return parseFixed(cl, vs, node, source)
	}

	if next, err := parseList(cl, vs, Changed, node.NextSibling(), source); err != nil {
		return err
	} else {
		return parseFixed(cl, vs, next, source)
	}
}

func parseFixed(cl *Changelog, vs *Version, node ast.Node, source []byte) error {
	if node == nil {
		return parseVersion(cl, node, source)
	}

	if !pContains(node, "fixed", source) {
		return parseDeprecated(cl, vs, node, source)
	}

	if next, err := parseList(cl, vs, Fixed, node.NextSibling(), source); err != nil {
		return err
	} else {
		return parseDeprecated(cl, vs, next, source)
	}
}

func parseDeprecated(cl *Changelog, vs *Version, node ast.Node, source []byte) error {
	if node == nil {
		return parseVersion(cl, node, source)
	}

	if !pContains(node, "deprecated", source) {
		return parseRemoved(cl, vs, node, source)
	}

	if next, err := parseList(cl, vs, Deprecated, node.NextSibling(), source); err != nil {
		return err
	} else {
		return parseRemoved(cl, vs, next, source)
	}
}

func parseRemoved(cl *Changelog, vs *Version, node ast.Node, source []byte) error {
	if node == nil {
		return parseVersion(cl, node, source)
	}

	if !pContains(node, "removed", source) {
		return parseSecurity(cl, vs, node, source)
	}

	if next, err := parseList(cl, vs, Removed, node.NextSibling(), source); err != nil {
		return err
	} else {
		return parseSecurity(cl, vs, next, source)
	}
}

func parseSecurity(cl *Changelog, vs *Version, node ast.Node, source []byte) error {
	if node == nil {
		return parseVersion(cl, node, source)
	}

	if !pContains(node, "security", source) {
		return parseVersion(cl, node, source)
	}

	if next, err := parseList(cl, vs, Security, node.NextSibling(), source); err != nil {
		return err
	} else {
		return parseVersion(cl, next, source)
	}
}

func parseList(cc changeContainer, vs *Version, t ChangeType, node ast.Node, source []byte) (ast.Node, error) {
	if node != nil {
		if l, ok := node.(*ast.List); ok {
			for child := l.FirstChild(); child != nil; child = child.NextSibling() {
				ch := Change{Version: vs, Type: t}

				if err := parseListItem(&ch, vs, t, child, source); err != nil {
					return nil, err
				}

				cc.AddChange(&ch)
			}

			return node.NextSibling(), nil
		}
	}

	return node, nil
}

func parseListItem(ch *Change, vs *Version, t ChangeType, node ast.Node, source []byte) error {
	for child := node.FirstChild(); child != nil; child = child.NextSibling() {
		if tb, ok := child.(*ast.TextBlock); ok {
			msg := tb.Lines().Value(source) // there should only be at most one TextBlock

			if idref := RefMarkdown.RefDeclExp().Find(msg); idref != nil {
				msg = msg[len(idref):]
			} else if idref = RefHtml.RefDeclExp().Find(msg); idref != nil {
				msg = msg[len(idref):]
			}

			ch.Message = msg
		} else if _, ok := child.(*ast.List); ok {
			if _, err := parseList(ch, vs, t, child, source); err != nil {
				return err
			}
		}
	}

	return nil
}

func pContains(node ast.Node, s string, source []byte) bool {
	if h, ok := node.(*ast.Paragraph); ok {
		t := h.Lines().Value(source)

		if strings.Contains(strings.ToLower(string(t)), s) {
			return true
		}
	}

	return false
}
