package changelog

import (
	"fmt"
	"io"
)

type ChangeType int

const (
	Added ChangeType = iota
	Changed
	Fixed
	Deprecated
	Removed
	Security
)

func (tp ChangeType) WriteTo(w io.Writer) (int64, error) {
	switch tp {
	case Added:
		n, err := fmt.Fprintf(w, "**%s:**%s", "Added", fmt.Sprintln())
		return int64(n), err
	case Changed:
		n, err := fmt.Fprintf(w, "**%s:**%s", "Changed", fmt.Sprintln())
		return int64(n), err
	case Fixed:
		n, err := fmt.Fprintf(w, "**%s:**%s", "Fixed", fmt.Sprintln())
		return int64(n), err
	case Deprecated:
		n, err := fmt.Fprintf(w, "**%s:**%s", "Deprecated", fmt.Sprintln())
		return int64(n), err
	case Removed:
		n, err := fmt.Fprintf(w, "**%s:**%s", "Removed", fmt.Sprintln())
		return int64(n), err
	case Security:
		n, err := fmt.Fprintf(w, "**%s:**%s", "Security", fmt.Sprintln())
		return int64(n), err
	}

	return 0, nil
}
