package changelog

import (
	"bytes"
	"fmt"
	"io"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/text"
)

type Changelog struct {
	Header      []byte
	Description [][]byte
	Changes     changeCollection
	RefType     RefType
	idMap       map[int]int
}

func NewChangeLog(header []byte, description [][]byte) *Changelog {
	return &Changelog{Header: header, Description: description, idMap: map[int]int{}}
}

func (cl *Changelog) Sort() {
	l := cl.Changes.Len()
	cl.Changes.Sort()

	for i, ch := range cl.Changes {
		ch.id = l - i
		cl.idMap[ch.origId] = ch.id
	}
}

func (cl *Changelog) AddChange(ch *Change) {
	cl.Changes = append(cl.Changes, ch)
	ch.changelog = cl
}

// ReadFrom reads and parses the input from the given reader into the Changelog.
func (cl *Changelog) ReadFrom(rd io.Reader) (int64, error) {
	var source []byte
	var buf bytes.Buffer
	var err error

	if source, err = io.ReadAll(rd); err != nil {
		return 0, err
	}

	if err = goldmark.Convert(source, &buf); err != nil {
		return 0, err
	}

	ast := goldmark.DefaultParser().Parse(text.NewReader(source))
	err = parseChangelog(cl, ast.FirstChild(), source)

	return 0, err
}

// WriteTo writes the Changelog to the given io.Writer in markdown format.
func (cl *Changelog) WriteTo(w io.Writer) (int64, error) {
	nl := fmt.Sprintln()
	var err error
	var n, k int64
	var m int

	m, err = fmt.Fprintf(w, "# %s%s", cl.Header, nl)
	n += int64(m)

	if err == nil {
		for _, p := range cl.Description {
			m, err = fmt.Fprintf(w, "%s%s%s", nl, p, nl)
			n += int64(m)

			if err != nil {
				break
			}
		}
	}

	if err == nil {
		var vs *Version
		var tp ChangeType = -1

		for _, ch := range cl.Changes {
			if ch.GetVersion() != vs {
				m, err = fmt.Fprintln(w)
				n += int64(m)

				if err == nil {
					vs = ch.GetVersion()
					tp = -1
					k, err = vs.WriteTo(w)
					n += k
				}
			}

			if ch.Type != tp {
				m, err = fmt.Fprintln(w)
				n += int64(m)

				if err == nil {
					tp = ch.Type
					k, err = tp.WriteTo(w)
					n += k

					if err == nil {
						m, err = fmt.Fprintln(w)
						n += int64(m)
					}
				}
			}

			if err == nil {
				k, err = ch.WriteTo(w)
				n += k
			}

			if err != nil {
				break
			}
		}
	}

	return n, err
}
