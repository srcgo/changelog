package changelog

import (
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
)

type Version struct {
	DateModified time.Time
	Semver       *semver.Version
}

var versionExp = regexp.MustCompile("^\\[(.+?)\\] - (\\d{4}).(\\d{2}).(\\d{2})$")

func (vs *Version) Parse(in []byte) error {
	match := versionExp.FindSubmatch(in)

	if len(match) < 5 {
		return fmt.Errorf("parser error in version header at '%s'", in)
	}

	var v *semver.Version
	var y, m, d int64
	var err error

	firstErr(
		func() error { v, err = semver.NewVersion(string(match[1])); return err },
		func() error { y, err = strconv.ParseInt(strings.TrimPrefix(string(match[2]), "0"), 0, 0); return err },
		func() error { m, err = strconv.ParseInt(strings.TrimPrefix(string(match[3]), "0"), 0, 0); return err },
		func() error { d, err = strconv.ParseInt(strings.TrimPrefix(string(match[4]), "0"), 0, 0); return err },
	)

	vs.DateModified = time.Date(int(y), time.Month(m), int(d), 0, 0, 0, 0, time.Local)
	vs.Semver = v
	return err
}

func (vs *Version) WriteTo(w io.Writer) (int64, error) {
	year, month, day := vs.DateModified.Date()
	n, err := fmt.Fprintf(w, "## [%s] - %04d-%02d-%02d%s", vs.Semver.String(), year, month, day, fmt.Sprintln())
	return int64(n), err
}
