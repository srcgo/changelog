package changelog

import (
	"fmt"
	"regexp"
)

type RefType int

const (
	RefNone RefType = iota
	RefMarkdown
	RefHtml
)

func (r RefType) RefDeclExp() *regexp.Regexp {
	return refDeclExps[r]
}

func (r RefType) RefInlExp() *regexp.Regexp {
	return refInlExps[r]
}

func (r RefType) RefDeclTmpl(entryId int) string {
	return refDeclTmpls[r](entryId)
}

func (r RefType) RefInlTmpl(entryId int) string {
	return refInlTmpls[r](entryId)
}

var refInlExps = map[RefType]*regexp.Regexp{
	// yes, they are all the same
	RefNone:     regexp.MustCompile(`^\*#(\d+)\*|\*#(\d+)\*$|[^\[]\*#(\d+)\*|\*#(\d+)\*[^\]]|\[\*#(\d+)\*\]\(#(\d+)\)`),
	RefMarkdown: regexp.MustCompile(`^\*#(\d+)\*|\*#(\d+)\*$|[^\[]\*#(\d+)\*|\*#(\d+)\*[^\]]|\[\*#(\d+)\*\]\(#(\d+)\)`),
	RefHtml:     regexp.MustCompile(`^\*#(\d+)\*|\*#(\d+)\*$|[^\[]\*#(\d+)\*|\*#(\d+)\*[^\]]|\[\*#(\d+)\*\]\(#(\d+)\)`),
}

var refDeclExps = map[RefType]*regexp.Regexp{
	RefNone:     regexp.MustCompile(`^`),
	RefMarkdown: regexp.MustCompile(`^\*#\d+\* `),
	RefHtml:     regexp.MustCompile(`^<a name="\d+"></a>\*#\d+\* `),
}

var refDeclTmpls = map[RefType]func(int) string{
	RefNone:     func(entryId int) string { return fmt.Sprintf(``) },
	RefMarkdown: func(entryId int) string { return fmt.Sprintf(`*#%d* `, entryId) },
	RefHtml:     func(entryId int) string { return fmt.Sprintf(`<a name="%d"></a>*#%d* `, entryId, entryId) },
}

var refInlTmpls = map[RefType]func(int) string{
	RefNone:     func(entryId int) string { return fmt.Sprintf(`*#%d*`, entryId) },
	RefMarkdown: func(entryId int) string { return fmt.Sprintf(`*#%d*`, entryId) },
	RefHtml:     func(entryId int) string { return fmt.Sprintf(`[*#%d*](#%d)`, entryId, entryId) },
	// entries are no headers hence inl-md-refs are invalid for md-ref-decls (but they are sufficient for html-ref-decls).
	// RefMarkdown: func(entryId int) string { return fmt.Sprintf(`[*#%d*](#%d)`, entryId, entryId) },
	// RefHtml:     func(entryId int) string { return fmt.Sprintf(`<a name="%d">*#%d*</a>`, entryId, entryId) },
}
