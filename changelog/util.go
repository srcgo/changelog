package changelog

import (
	"sort"
	"strings"
)

type changeContainer interface {
	AddChange(*Change)
}

type changeCollection []*Change

func (cc changeCollection) Len() int      { return len(cc) }
func (cc changeCollection) Swap(i, j int) { cc[i], cc[j] = cc[j], cc[i] }
func (cc changeCollection) Less(i, j int) bool {
	c := cc[i].GetVersion().Semver.Compare(cc[j].GetVersion().Semver)
	return c > 0 || (c == 0 && (cc[i].Type < cc[j].Type || (cc[i].Type == cc[j].Type && (strings.Compare(string(cc[i].Message), string(cc[j].Message)) < 0))))
}

func (cc changeCollection) Sort() {
	sort.Sort(cc)

	for _, ch := range cc {
		ch.Children.Sort()
	}
}

func firstErr(fns ...func() error) error {
	var err error

	for _, fn := range fns {
		err = fn()

		if err != nil {
			return err
		}
	}

	return nil
}
