package changelog

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

type Change struct {
	Type       ChangeType
	Version    *Version
	Message    []byte
	Children   changeCollection
	parent     *Change
	changelog  *Changelog
	origId, id int
}

// TODO: proper and safer interfaces (this is just an example)
func (ch *Change) SetVersion(vs *Version) {
	if ch.parent != nil {
		panic("cannot change version of nested changes")
	}

	ch.Version = vs
}

func (ch *Change) GetLevel() int {
	if ch.parent != nil {
		return ch.parent.GetLevel() + 1
	}

	return 0
}

func (ch *Change) GetVersion() *Version {
	if ch.parent != nil {
		return ch.parent.GetVersion()
	}

	return ch.Version
}

func (ch *Change) GetChangelog() *Changelog {
	if ch.parent != nil {
		return ch.parent.GetChangelog()
	}

	return ch.changelog
}

// TODO: maybe instantiate new Change here (instead of receiving one)
func (ch *Change) AddChange(child *Change) {
	ch.Children = append(ch.Children, child)
	child.parent = ch
}

// WriteTo writes the Change message to the given io.Writer in markdown format.
func (ch *Change) WriteTo(w io.Writer) (int64, error) {
	var err error
	var n, k int64
	var m int

	refType := ch.GetChangelog().RefType
	var level = ch.GetLevel()
	var refDecl string

	if level == 0 {
		refDecl = refType.RefDeclTmpl(ch.id)
	}

	m, err = fmt.Fprintf(w, "%s- %s%s%s", strings.Repeat("  ", level), refDecl, ch.replaceRefs(), fmt.Sprintln())
	n += int64(m)

	if err == nil {
		for _, child := range ch.Children {
			k, err = child.WriteTo(w)
			n += k

			if err != nil {
				break
			}
		}
	}

	return n, err
}

func (ch *Change) replaceRefs() []byte {
	cl := ch.GetChangelog()
	idmap := cl.idMap
	refType := cl.RefType
	exp := refType.RefInlExp()

	return exp.ReplaceAllFunc(ch.Message, func(match []byte) []byte {
		matches := exp.FindSubmatch(match)

		if len(matches) > 1 && matches[1] != nil {
			if id, err := strconv.Atoi(string(matches[1])); err == nil {
				if newId, has := idmap[id]; has {
					return []byte(refType.RefInlTmpl(newId))
				}
			}
		} else if len(matches) > 2 && matches[2] != nil {
			if id, err := strconv.Atoi(string(matches[2])); err == nil {
				if newId, has := idmap[id]; has {
					return []byte(refType.RefInlTmpl(newId))
				}
			}
		} else if len(matches) > 3 && matches[3] != nil {
			if id, err := strconv.Atoi(string(matches[3])); err == nil {
				if newId, has := idmap[id]; has {
					return []byte(fmt.Sprintf("%c%s", []rune(string(match))[0], refType.RefInlTmpl(newId)))
				}
			}
		} else if len(matches) > 4 && matches[4] != nil {
			if id, err := strconv.Atoi(string(matches[4])); err == nil {
				if newId, has := idmap[id]; has {
					return []byte(fmt.Sprintf("%s%c", refType.RefInlTmpl(newId), []rune(string(match))[len([]rune(string(match)))-1]))
				}
			}
		} else if len(matches) > 5 && matches[5] != nil && string(matches[5]) == string(matches[6]) {
			if id, err := strconv.Atoi(string(matches[5])); err == nil {
				if newId, has := idmap[id]; has {
					return []byte(refType.RefInlTmpl(newId))
				}
			}
		}

		return match
	})
}
