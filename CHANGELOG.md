# Changelog

## [2.0.2-beta] - 2025-01-08

**Fixed:**

- *#11* Adding entries to empty changelog not creating a default version
- *#10* Changelog file being created with wrong file permissions

## [2.0.1-beta] - 2025-01-05

**Added:**

- *#9* Support for `--increment` flag in `update` command

**Fixed:**

- *#8* `--end` flag in `cat` command stopping output after first entry of last version

## [2.0.0-beta] - 2025-01-05

**Added:**

- *#7* Reference support (entry ids are based of their order)
  - `html` hyperlink-references
  - `markdown` conform *references* (no linking, just visual)
  - use either `*#<id>*` or `[*#<id>*](#<id>)` to reference other entries inline (auto update if format or order of entries was changed)

**Changed:**

- *#6* Rework of changelog api and cli
  - Improved command line interface powered by `github.com/urfave/cli` (v2) (see *#5*)
  - Improved markdown parsing powered by `github.com/yuin/goldmark`
  - Improved semver parsing powered by `github.com/Masterminds/semver` (v3)
- *#5* Reworked cli commands and flags (use `changelog COMMAND --help` for more info)
  - add command `add entry`
  - add command `add note`
  - add command `cat`
  - add command `delete`
  - add command `update`

## [1.0.2] - 2024-06-24

**Added:**

- *#4* `--cat [n]` flag for cli tool

## [1.0.1] - 2024-05-09

**Fixed:**

- *#3* Issue with dates being parsed as octal values

## [1.0.0] - 2024-03-26

**Added:**

- *#2* Changelog API (in alignment with [keepachangelog](https://keepachangelog.com/en/1.0.0/))
- *#1* Changelog Command Line Tool
