# changelog

`changelog` is a simple command line tool to create and manage changelog files. Inspired by [keepachangelog](https://keepachangelog.com/en/1.0.0/).

Keep your changelogs in a uniform layout with well-formated *markdown* syntax.

## Install

**Binary:**

```bash
go install gitlab.com/srcgo/changelog/cmd/changelog@latest
```

**API:**

```bash
go get gitlab.com/srcgo/changelog
```

## Description

Output of `changelog --help`:

```text
NAME:
   changelog - manage a changelog file

USAGE:
   changelog [GLOBAL OPTIONS] [COMMAND [COMMAND OPTIONS]]

VERSION:
   2.0.0-beta

DESCRIPTION:
   The accessed changelog file is specified by the CHANGELOG_FILE
   environment variable, which defaults to 'CHANGELOG.md' if not set.
   
   A changelog file may contain several paragraphs for a description
   below the main header, which are preserved during any invocations
   of the changelog command.
   
   Examples:
   
   changelog add entry -t added -m "new feature" -i minor --date
   changelog add entry -t changed -m "forgotten feature" -v 0.1.5
   changelog add note -m "some note" --entry 2
   changelog update -v 0.1.0 --target 0.2.0
   changelog update --date
   changelog delete -v 0.0.2 --entry 2 --version 0.0.3
   changelog cat --start 1 -e -2 --refs html

COMMANDS:
   add     add an entry or a note
   update  update a version
   delete  delete versions and/or entries
   cat     output the changelog

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```
